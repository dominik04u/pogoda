package com.example.hp.pogoda;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.res.Configuration;
import android.os.Bundle;


public class MainActivity extends Activity {

    FragmentManager fragmentManager;
    FragmentTransaction fragmentTransaction;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
            setContentView(R.layout.nowy);
        }
        else{
            setContentView(R.layout.container);
            getFragmentManager().beginTransaction()
                    .replace(R.id.container, new ListaFragment()).addToBackStack(null)
                    .commit();
        }
    }
}